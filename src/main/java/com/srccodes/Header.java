package com.srccodes;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
@Entity
@Table(name = "header")
public class Header {
	
	@Id
	private Integer id;
	
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="header_id", referencedColumnName="id")
	@Where(clause="_column1 = '14' and column2 = '25'")
	private Set<Line> lines;
	
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="header_id", referencedColumnName="id")
	@Where(clause="_column1 = '13' and column2 = '26'")
	private Set<OtherLine> otherLines;

}
