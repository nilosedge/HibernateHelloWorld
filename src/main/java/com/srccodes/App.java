package com.srccodes;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;

public class App {

	public static void main(String[] args) {
		Configurator.setRootLevel(Level.DEBUG);
		
		Map<String, String> persistenceMap = new HashMap<String, String>();
		persistenceMap.put("javax.persistence.jdbc.url", "jdbc:sqlite:db.sqlite");
		
		EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("default");
		EntityManager em = managerFactory.createEntityManager();

		em.find(Header.class, 1);
	}

}
