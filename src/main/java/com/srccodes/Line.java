package com.srccodes;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
@Entity
@Table(name = "line")
public class Line {
	@Id
	private Integer id;
	private Integer header_id;
	private String _column1;
	private String column2;
}