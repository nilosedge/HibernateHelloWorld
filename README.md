

When generating table alias names, if the column in a @Where clause starts with an underscore, then the alias for the table is omitted from the query. 

<pre>
@Entity
@Table(name = "header")
public class Header {
	@Id
	private Integer id;
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="header_id", referencedColumnName="id")
	@Where(clause="column2 = '13' and _column1 = '13'")
	private Set<Line> lines;
}
@Entity
@Table(name = "line")
public class Line {
	@Id
	private Integer id;
	private Integer header_id;
	private String _column1;
	private String column2;
}

EntityManager em = managerFactory.createEntityManager();
em.find(Header.class, 1);
</pre>

Generated query:

<pre>
Hibernate: 
    select
        header0_.id as id1_0_0_,
        lines1_.header_id as header_i4_1_1_,
        lines1_.id as id1_1_1_,
        lines1_.id as id1_1_2_,
        lines1_._column1 as _column2_1_2_,
        lines1_.column2 as column3_1_2_,
        lines1_.header_id as header_i4_1_2_ 
    from
        header header0_ 
    left outer join
        line lines1_ 
            on header0_.id=lines1_.header_id 
            and (
                lines1_.column2 = '13' 
                and _column1 = '13'
            )  
    where
        header0_.id=?
</pre>

Column \_column1 is missing the lines1_ alias.

Where this is really a problem is when you have a relationship like the following:

<pre>
@Entity
@Table(name = "header")
public class Header {
	@Id
	private Integer id;
	
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="header_id", referencedColumnName="id")
	@Where(clause="_column1 = '14'")
	private Set<Line> lines;
	
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="header_id", referencedColumnName="id")
	@Where(clause="_column1 = '13'")
	private Set<OtherLine> otherLines;
}

@Entity
@Table(name = "line")
public class Line {
	@Id
	private Integer id;
	private Integer header_id;
	private String _column1;
	private String column2;
}

@Entity
@Table(name = "otherline")
public class OtherLine {
	@Id
	private Integer id;
	private Integer header_id;
	private String _column1;
	private String column2;
}
</pre>

Generated query:

<pre>
Hibernate: 
    select
        header0_.id as id1_0_0_,
        lines1_.header_id as header_i4_1_1_,
        lines1_.id as id1_1_1_,
        lines1_.id as id1_1_2_,
        lines1_._column1 as _column2_1_2_,
        lines1_.column2 as column3_1_2_,
        lines1_.header_id as header_i4_1_2_,
        otherlines2_.header_id as header_i4_2_3_,
        otherlines2_.id as id1_2_3_,
        otherlines2_.id as id1_2_4_,
        otherlines2_._column1 as _column2_2_4_,
        otherlines2_.column2 as column3_2_4_,
        otherlines2_.header_id as header_i4_2_4_ 
    from
        header header0_ 
    left outer join
        line lines1_ 
            on header0_.id=lines1_.header_id 
            and (
                _column1 = '14'
            )  
    left outer join
        otherline otherlines2_ 
            on header0_.id=otherlines2_.header_id 
            and (
                _column1 = '13'
            )  
    where
        header0_.id=?
</pre>

Both _column1 are missing their alias and cause the following exception:

SQL error or missing database (ambiguous column name: _column1)

Most people don't use underscore's in their table names. I am working with a database where ALL primary keys to the tables start with underscore and I have to jump through modeling hoops in order to get these to work.

In the following case:

<pre>
@Entity
@Table(name = "header")
public class Header {
	
	@Id
	private Integer id;
	
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="header_id", referencedColumnName="id")
	@Where(clause="_column1 = '14' and column2 = '25'")
	private Set<Line> lines;
	
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="header_id", referencedColumnName="id")
	@Where(clause="_column1 = '13' and column2 = '26'")
	private Set<OtherLine> otherLines;

}
</pre>

The generated query is the following:

<pre>
Hibernate: 
    select
        header0_.id as id1_0_0_,
        lines1_.header_id as header_i4_1_1_,
        lines1_.id as id1_1_1_,
        lines1_.id as id1_1_2_,
        lines1_._column1 as _column2_1_2_,
        lines1_.column2 as column3_1_2_,
        lines1_.header_id as header_i4_1_2_,
        otherlines2_.header_id as header_i4_2_3_,
        otherlines2_.id as id1_2_3_,
        otherlines2_.id as id1_2_4_,
        otherlines2_._column1 as _column2_2_4_,
        otherlines2_.column2 as column3_2_4_,
        otherlines2_.header_id as header_i4_2_4_ 
    from
        header header0_ 
    left outer join
        line lines1_ 
            on header0_.id=lines1_.header_id 
            and (
                _column1 = '14' 
                and lines1_.column2 = '25'
            )  
    left outer join
        otherline otherlines2_ 
            on header0_.id=otherlines2_.header_id 
            and (
                _column1 = '13' 
                and otherlines2_.column2 = '26'
            )  
    where
        header0_.id=?
 </pre>
 
 Column2 gets the alias just fine but _column1 does not.
 
 

